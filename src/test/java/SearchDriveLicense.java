import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static java.lang.System.err;

public class SearchDriveLicense extends BaseClass {


    @Test
    public void testDriveLicenseRequest() {
        wd.get("https://www.gosuslugi.ru/");
        // Выбираем форму поиска, заполняем её и отправляем запрос. Сам запрос параметризован (задел на будущее).
        fillSearchInformation("Патент");
        /*
        Проверяем, что выбрано "Все" -- с локатором здесь сложности: можно привязаться к слову "все", но насколько
      это корректно? По большому счету строки идентичные и разница при проставлении active только в русском наименовании оных... и насколько будет подходящим для этой проверки подобный метод: !driver.findElements(By.id("...")).isEmpty(); ? */
        checkingSearchResult();


    }



    public void fillSearchInformation(String searchquery) {
        wd.findElement(By.xpath("//div[@id='_epgu_el1']/input")).click();
        wd.findElement(By.xpath("//div[@id='_epgu_el1']/input")).clear();
        wd.findElement(By.xpath("//div[@id='_epgu_el1']/input")).sendKeys(searchquery);
        wd.findElement(By.xpath("//div[contains(@class,'searchBox')]//a[@class='btn-base medium search']")).click();


    }

    public void checkingSearchResult() {
        List<WebElement> searchList = wd.findElements(By.xpath("//div[@ng-if='results && results.length']"));
       assert (searchList.size() < 20);

          }
        }

